<?php
class Produk_model extends CI_Model{
    public $id_produk;
    public $nama_produk;
    public $harga_produk;
    public $ketersediaan;
    public $gambar;
    public $deskripsi;
    

    public function getShoesproduk()
    {
        $this->load->database();
        $produk = $this->db->get("produk");
        $result = $produk->result();
        return json_encode($result);
    }
}